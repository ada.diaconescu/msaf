% BiochemicalOscillators_OneLevelFlat
% Extending Kim's model to multiple oscillators on the same level (which are all oscillators)

% This version loops through multiple values of f and tau and saves data to
% a file.

tic

close all
clear all

N = 11;          % number of oscillators
t_end = 1000;    % how long to run the simulation
t_size = 100;   % size of time window for plot
t_start = t_end-t_size;
num_of_trials = 10;

PP = 1;         % 1 - use the PP model, 0 - use the NN model

% Define constants
H = 3;
VX = 1;
VY = 1;
KX = 0.5;
KY = 0.5;
KdX = 0.5;
KdY = 0.5;
KbX = 0.1;
KbY = 0.1;
tauXY = 2;
tauYX = 2;

f = 0:8;
TD = 1:15;
A = zeros(length(f)*length(TD),11);
count = 1;

% Loop through values for F and tau
for F = f
    for tau = TD
        params = [H,VX,VY,KX,KY,KdX,KdY,KbX,KbY,N,PP,F];
        delays = [tauXY,tauYX,tau];

        per_ave = zeros(1,num_of_trials);
        pp_ave = zeros(1,num_of_trials);
        synch = zeros(1,num_of_trials);
        synch_time = zeros(1,num_of_trials);
        
        for j = 1:num_of_trials

            % Initial conditions
            IC = ones(1,2*N);
            IC(1:N) = rand(1,N);

            % xy = [X(M,1),X(M-1,1),...,X(M-1,G),X(M-2,1),...,X(M-2,G^2),...,X(1,1),...,X(1,G^(M-1)),Y(M,1),Y(M-1,1),...,Y(M-1,G),Y(M-2,1),...,Y(M-2,G^2),Y(1,1),...,Y(1,G^(M-1))]
            % All X's grouped together then all Y's grouped together, start at top layer
            % Two groups, each with N members
            % For X at position k, its corresponding Y is at k+N
            sol = dde23(@(t,xy,Z) ddefun(t,xy,Z,params),delays,IC,[0, t_end]);
            t = sol.x;

            if PP == 1
                fbk = 'PP';
            else
                fbk = 'NN';
            end

            t_peak = zeros(1,N);
            per = zeros(1,N);
            pp = zeros(1,N);

            clear x y LOC
            for n = 1:N
                x(n,:) = sol.y(n,:);
                y(n,:) = sol.y(N+n,:);
%                 if j == num_of_trials
%                     plot(t,x(n,:))
%                     hold on
%                 end
                [pks,loc] = findpeaks(x(n,:));
                if n > 1
                    if length(loc) < length(LOC(n-1,:))
                        LOC = LOC(:,end-length(loc)+1:end);
                        LOC(n,:) = loc;
                    else
                        LOC(n,:) = loc(end-length(LOC(n-1,:))+1:end);
                    end
                else
                    LOC(n,:) = loc;
                end
                t_peak(n) = t(loc(end));
                per(n) = t(loc(end))-t(loc(end-1));
                pp(n) = max(x(n,end-t_size:end))-min(x(n,end-t_size:end));
            end
            per_ave(j) = mean(per);
            pp_ave(j) = mean(pp);
            
            if length(unique(t_peak)) == 1
                synch(j) = 1;
                synch_index = 1;
                synched = 0;
                while synched == 0
                    if (length(unique(LOC(:,synch_index))) == 1)
                        synched = 1;
                    else
                        synch_index = synch_index+1;
                    end
                end
                synch_time(j) = t(LOC(1,synch_index));
            else
                synch(j) = 0;
                synch_time(j) = 0;
            end          
        end   % j
          
        A(count,:) = [N,PP,F,tau,mean(synch),mean(per_ave),std(per_ave),mean(pp_ave),std(pp_ave),mean(synch_time),var(synch_time)];
        count = count+1
        
    end   %TD
end   % f

top_row = ["N","Type","F","tau","Synch?","Average Period","StdDev Period","Average PP Amp","StdDev PP Amp","Average Synch Time","StdDev Synch Time"];
B = [top_row;A];
writematrix(B,datestr(datetime('now'),30))

disp("Done!")
toc
% --------------------------------------------------------------------------

function dxydt = ddefun(t,xy,Z,p)

    % params = [H,VX,VY,KX,KY,KdX,KdY,KbX,KbY,N,PP,F];  (F is scalar)
    H = p(1);
    VX = p(2);
    VY = p(3);
    KX = p(4);
    KY = p(5);
    KdX = p(6);
    KdY = p(7);
    KbX = p(8);
    KbY = p(9);
    N = p(10);  % total number of oscillators
    PP = p(11);
    F = p(12);
    
    % Z(state variable index, delay index)
    % delays = [tauXY,tauYX,tau];  (tau is scalar)   
    
    dxydt = zeros(N,1);
    
    for n = 1:N                 % n is child number within ring
        if n == 1
            Xtau = Z(N,3);
        else
            Xtau = Z(n-1,3);
        end
        XtauXY = Z(n,1);
        YtauYX = Z(N+n,2);
        dxydt(n) = VX*(1+PP*(F*Xtau)^H)/(1+(F*Xtau)^H + (YtauYX/KY)^H) - KdX*xy(n) + KbX;   % X
        dxydt(N+n) = VY*(XtauXY/KX)^H/(1+(XtauXY/KX)^H) - KdY*xy(N+n) + KbY;   % Y 
    end
end