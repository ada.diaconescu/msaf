% BiochemicalOscillators_MultipleLevels
% Extending Kim's model to have many levels (which are all oscillators)

% This version loops through multiple values of f and tau and saves data to
% a file.

tic

close all
clear all

M = 4;          % number of layers
C = 4;          % number of children
t_end = 1000;    % how long to run the simulation
t_size = 100;   % size of time window for plot
t_start = t_end-t_size;

PP = 0;         % 1 - use the PP model, 0 - use the NN model

% Define constants
H = 3;
VX = 1;
VY = 1;
KX = 0.5;
KY = 0.5;
KdX = 0.5;
KdY = 0.5;
KbX = 0.1;
KbY = 0.1;
tauXY = 2;
tauYX = 2;

% Calculate total number of oscillators, number at bottom layer, start
% index of each layer
N = 1;
level_start = ones(1,M);
for m = 1:M-1
    N = N + C^m;
    level_start(m+1) = level_start(m) + C^(m-1);
end
N_bottom = C^(M-1);

f_vector = 0:8;
TD_vector = 1:15;
A = zeros(length(f_vector)*length(TD_vector),14);
count = 1;

% Loop through values for F and tau
for f = f_vector
    for TD = TD_vector
        % One value per level, first entry is value for top level
        F = f*ones(1,M);    % coupling strength (range 0-8)
        W = 0.5;      % weighting factor (W = 0: only use input from below, W = 1: only use input from above)
        tau = TD*ones(1,M);  % delay (range 0-15)

        params = [H,VX,VY,KX,KY,KdX,KdY,KbX,KbY,M,C,N,PP,F,W];
        delays = [tauXY,tauYX,tau];

        num_of_trials = 10;
        per_ave = zeros(1,num_of_trials);
        pp_ave = zeros(1,num_of_trials);
        synch = zeros(1,num_of_trials);
        inphase = zeros(1,num_of_trials);
        synch_time = zeros(1,num_of_trials);
        
        for j = 1:num_of_trials

            % Initial conditions
            IC = ones(1,2*N);
            IC(1:N) = rand(1,N);

            % xy = [X(M,1),X(M-1,1),...,X(M-1,G),X(M-2,1),...,X(M-2,G^2),...,X(1,1),...,X(1,G^(M-1)),Y(M,1),Y(M-1,1),...,Y(M-1,G),Y(M-2,1),...,Y(M-2,G^2),Y(1,1),...,Y(1,G^(M-1))]
            % All X's grouped together then all Y's grouped together, start at top layer
            % Two groups, each with N members
            % For X at position k, its corresponding Y is at k+N
            sol = dde23(@(t,xy,Z) ddefun(t,xy,Z,params),delays,IC,[0, t_end]);
            t = sol.x;

            if PP == 1
                fbk = 'PP';
            else
                fbk = 'NN';
            end

            t_peak = zeros(1,N);
            per = zeros(1,N);
            pp = zeros(1,N);

            clear x y LOC
            for n = 1:N
                x(n,:) = sol.y(n,:);
                y(n,:) = sol.y(N+n,:);
%                 if j == num_of_trials
%                     plot(t,x(n,:))
%                     hold on
%                 end
                [pks,loc] = findpeaks(x(n,:));
                if n > 1
                    if length(loc) < length(LOC(n-1,:))
                        LOC = LOC(:,end-length(loc)+1:end);
                        LOC(n,:) = loc;
                    else
                        LOC(n,:) = loc(end-length(LOC(n-1,:))+1:end);
                    end
                else
                    LOC(n,:) = loc;
                end
                t_peak(n) = t(loc(end));
                per(n) = t(loc(end))-t(loc(end-1));
                pp(n) = max(x(n,end-t_size:end))-min(x(n,end-t_size:end));
            end
            per_ave(j) = mean(per(end-N_bottom+1:end));
            pp_ave(j) = mean(pp(end-N_bottom+1:end));
            
            synch(j) = (length(unique(t_peak(end-N_bottom+1:end))) == 1);
            if synch(j) == 0
                synch_time(j) = 0;
                inphase(j) = 0;
            else
                synch_index = 1;
                synched = 0;
                while synched == 0
                    if (length(unique(LOC(end-N_bottom+1:end,synch_index))) == 1)
                        synched = 1;
                    else
                        synch_index = synch_index+1;
                    end
                end
                synch_time(j) = t(LOC(1,synch_index));            
                inphase(j) = 1;
                for m = 1:M-1
                    inphase(j) = inphase(j) && (t_peak(m) == t_peak(m+1));
                end
            end            
        end   % j
        
%         synch
%         inphase
%         per_ave
%         pp_ave
        
        A(count,:) = [M,C,PP,f,TD,W,mean(synch),mean(per_ave),std(per_ave),mean(pp_ave),std(pp_ave),mean(inphase),mean(synch_time),var(synch_time)];
        count = count+1
        
    end   %TD
end   % f

top_row = ["M","C","Type","F","tau","W","Synch?","Average Period","StdDev Period","Average PP Amp","StdDev PP Amp","Layers in Phase?","Average Synch Time","StdDev Synch Time"];
B = [top_row;A];
writematrix(B,datestr(datetime('now'),30))

% grid
% title(['All Xs plotted together: M = ',num2str(M),', C = ',num2str(C),', F = ',num2str(F),', \tau = ',num2str(tau),', W = ',num2str(W),', ',fbk])
% labels = 1:N;
% legend(num2str(labels'))
% xlabel('Time')
% 
% xmax = max(max(x));
% xmin = min(min(x));
% axis([t_start,t_end,xmin,xmax])
    
% figure
% level_start = 1;
% for m = 1:M
%     if m > 1
%         level_start = level_start + C^(m-2);
%     end
%     subplot(M,1,m)
%     hold on
%     for n = 0:C^(m-1)-1
%         plot(t,x(level_start+n,:))
%     end
%     hold off
%     axis([t_start,t_end,xmin,xmax])
%     labels = 1:C^(m-1);
%     legend(num2str(labels'))
%     ylabel(m)
%     grid
% end
% subplot(M,1,1),title(['Xs at each level: M = ',num2str(M),', C = ',num2str(C),', F = ',num2str(F),', \tau = ',num2str(tau),', W = ',num2str(W),', ',fbk])
% subplot(M,1,M),xlabel('Time')

% figure
% level_start = 1;
% for m = 1:M
%     if m > 1
%         level_start = level_start + C^(m-2);
%         s = C^(M-m);
%         for c = 1:C^(m-2)
%             strt = (m-1)*C^(M-2)+1+(c-1)*s;
%             subplot(M,C^(M-2),[strt:strt+s-1])
%             hold on
%             for n = 0:C-1
%                 k = level_start+(c-1)*C+n;
%                 plot(t,x(k,:))
%             end
%             hold off
%             grid
%             axis([t_start,t_end,xmin,xmax])
%         end
%     else
%         subplot(M,C^(M-2),[1:C^(M-2)])
%         plot(t,x(1,:))
%         grid
%         axis([t_start,t_end,xmin,xmax])
%         title(['Xs grouped at each level: M = ',num2str(M),', C = ',num2str(C),', F = ',num2str(F),', \tau = ',num2str(tau),', W = ',num2str(W),', ',fbk])
%     end
% end

disp("Done!")
toc
% --------------------------------------------------------------------------

function dxydt = ddefun(t,xy,Z,p)

    % params = [H,VX,VY,KX,KY,KdX,KdY,KbX,KbY,M,C,N,PP,F,W];  (F is 1 x M)
    H = p(1);
    VX = p(2);
    VY = p(3);
    KX = p(4);
    KY = p(5);
    KdX = p(6);
    KdY = p(7);
    KbX = p(8);
    KbY = p(9);
    M = p(10);  % number of layers
    C = p(11);  % number of children
    N = p(12);  % total number of oscillators
    PP = p(13);
    F = p(14:end-1);
    W = p(end);
    
    % Z(state variable index, delay index)
    % delays = [tauXY,tauYX,tau];  (tau is 1 x M)   
    
    dxydt = zeros(N,1);
    Xtau = zeros(N,1);
    XtauXY = zeros(N,1);
    YtauYX = zeros(N,1);
    
    level_start = ones(1,M);
    for m = 2:M
        level_start(m) = level_start(m-1) + C^(m-2);
    end
    
    % Top layer
    Xavetau = 0;
    for k = 2:C+1
        Xavetau = Xavetau+Z(k,3);
    end
    Xavetau = Xavetau/C;
    XtauXY(1) = Z(1,1);
    YtauYX(1) = Z(N+1,2);
    dxydt(1) = VX*(1+PP*(F(1)*Xavetau)^H)/(1+(F(1)*Xavetau)^H + (YtauYX(1)/KY)^H) - KdX*xy(1) + KbX;   % top level X
    dxydt(N+1) = VY*(XtauXY(1)/KX)^H/(1+(XtauXY(1)/KX)^H) - KdY*xy(N+1) + KbY;   % top level Y

    % Now do the middle layers
    for m = 2:M-1
        for g = 0:C^(m-2)-1             % g is group number within layer m
            kp = level_start(m-1)+g;    % index of parent
            Xtau(kp) = Z(kp,1+m);
            for n = 0:C-1               % n is child number within group g
                i = g*C+n;              % oscillator number within layer m
                k = level_start(m)+i;   % index of current oscillator X
                XtauXY(k) = Z(k,1);
                YtauYX(k) = Z(N+k,2);
                % Calculate average concentration of children
                Xavetau = 0;
                for c = i*C:i*C+C-1
                    kc = level_start(m+1)+c;
                    Xavetau = Xavetau + Z(kc,1+m);
                end
                Xavetau = Xavetau/C;
                dxydt(k) = VX*(1+PP*(F(m)*(W*Xtau(kp)+(1-W)*Xavetau))^H)/(1+(F(m)*(W*Xtau(kp)+(1-W)*Xavetau))^H + (YtauYX(k)/KY)^H) - KdX*xy(k) + KbX;   % X
                dxydt(N+k) = VY*(XtauXY(k)/KX)^H/(1+(XtauXY(k)/KX)^H) - KdY*xy(N+k) + KbY;   % Y                
            end
        end
    end
    
    % Bottom layer
    for g = 0:C^(M-2)-1             % g is group number within layer m
        kp = level_start(M-1)+g;    % index of parent
        Xtau(kp) = Z(kp,1+M);
        for n = 0:C-1               % n is child number within group g
            i = g*C+n;              % oscillator number within layer m
            k = level_start(M)+i;   % index of current oscillator X
            XtauXY(k) = Z(k,1);
            YtauYX(k) = Z(N+k,2);
            dxydt(k) = VX*(1+PP*(F(M)*Xtau(kp))^H)/(1+(F(M)*Xtau(kp))^H + (YtauYX(k)/KY)^H) - KdX*xy(k) + KbX;   % X
            dxydt(N+k) = VY*(XtauXY(k)/KX)^H/(1+(XtauXY(k)/KX)^H) - KdY*xy(N+k) + KbY;   % Y 
        end
    end
end